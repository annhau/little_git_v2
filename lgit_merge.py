import difflib
import time

import lgit_checkout
from lgit_commit import get_last_commit, set_last_commit, \
    save_commit_information
from lgit_helper import *


def diff_of_files(file1, file2, branch):
    """
    This function receive 2 files and return merged conflict version of them
    If a file contains the other, return that one
    Else, find differnces between them and show them as GIT does.
    """

    # Read files
    with open(file1) as f1:
        with open(file2) as f2:
            f1_content = f1.read()
            f2_content = f2.read()

    # Find diffs, remove '?'-lines
    diffs = difflib.ndiff(f1_content.split('\n'), f2_content.split('\n'))
    diffs = [x for x in diffs if x[0] != '?']

    # If there's only 2 unique patterns, return full content of a file
    all_patterns = set([x[0] for x in diffs])
    if all_patterns == {' ', '+'}:
        return f2_content
    elif all_patterns == {' ', '-'}:
        return f1_content
    else:
        # File diff line of both files
        content = []
        i = 0
        while i < len(diffs):
            this_line_sign = diffs[i][0]
            next_line_sign = diffs[i + 1][0] if i + 1 < len(diffs) else ' '
            paragraph = [diffs[i]]
            if this_line_sign.strip() and next_line_sign.strip():
                while i + 1 < len(diffs) and this_line_sign.strip() \
                        and next_line_sign.strip():
                    paragraph += [diffs[i + 1]]
                    i += 1
                    this_line_sign = next_line_sign
                    next_line_sign = diffs[i + 1][0] if i + 1 < len(
                        diffs) else ' '
            if paragraph and len(paragraph) > 1:
                src = [x[2:] for x in paragraph if x[0] == '-']
                dest = [x[2:] for x in paragraph if x[0] == '+']
                content.append("<<<<<<< HEAD")
                content += src
                content.append("=======")
                content += dest
                content.append(">>>>>>> " + branch)
            else:
                content.append(paragraph[0][2:])
            i += 1
        return '\n'.join(content)


def load_tracking_dict():
    """
    Tracking dict saves commit -> previous commit
    This function load our tracking file and return a dict
    """
    result = {}
    with open(TRACKING_PATH) as f:
        for line in f:
            key, value = line.strip("\n").split()
            result[key] = value
    return result


def load_snapshot(branch):
    """
    Load last commit's snapshot of a branch
    Return a dict {file_name: file_hash}
    """
    last_commit = get_last_commit(branch)
    snapshots = {}
    with open("%s/%s" % (SNAPSHOTS_PATH, last_commit)) as f:
        for line in f:
            _hash, file_name = line.split()
            snapshots[file_name] = _hash
    return snapshots


def is_feed_forward(dic, src, dest):
    """
    Verify if a branch and other is feed forward
    """
    while dest in dic:
        dest = dic[dest]
        if dest == src:
            return True
    return False


def merge(branch):
    """
    Receive a branch name and merge it to current branch (HEAD)
    Workflow of merge:
    - If HEAD and branch is feed forward and HEAD before branch:
        Move refs of HEAD to branch
    - Else if branch before: Nothing to do
    - If they ain't feed forward, check if they have any conflict
    (difference version of the same file):
        If it does, give them the instruction to merge their conflict
        Else auto-merge those branches.
    """
    update_index()
    author = get_author_name()
    if not author:
        print("Who are you?")
        return

    head_branch = get_head()
    tracking_dict = load_tracking_dict()
    if is_feed_forward(tracking_dict, head_branch, branch):
        # Feed forward, copy refs of new branch to HEAD branch
        # Do some checkout tricks to reverse current dicrectory
        set_last_commit(head_branch, get_last_commit(branch))
        lgit_checkout.checkout(branch)
        lgit_checkout.checkout(head_branch)
    else:
        if is_feed_forward(tracking_dict, branch, head_branch):
            print("Branch is up-to-date")
        else:
            # Not feed-forward, true merge
            # Load list of file of 2 branches in format {filename:filehash}
            snapshot_head = load_snapshot(head_branch)
            snapshot_branch = load_snapshot(branch)
            new_commit = []

            # If files are not common, move them to curr dir
            common_files = set()
            for key in snapshot_head:
                if key not in snapshot_branch:
                    # Copy file from object to current dir
                    _hash = snapshot_head[key]
                    _path = OBJECT_PATH + '/' + _hash[:2] + '/' + _hash[2:]
                    copy_file(_path, key)
                    new_commit.append(_hash + ' ' + key)  # hash + filename
                else:
                    # If their content is similar, add to CONFLICT, else add
                    #  to new_commit
                    if snapshot_branch[key] != snapshot_head[key]:
                        common_files.add(key)
                    else:
                        new_commit.append(snapshot_branch[key] + ' ' + key)

            for key in snapshot_branch:
                if key not in snapshot_head:
                    _hash = snapshot_branch[key]
                    _path = OBJECT_PATH + '/' + _hash[:2] + '/' + _hash[2:]
                    copy_file(_path, key)
                    new_commit.append(_hash + ' ' + key)
                else:
                    # If their content is similar, add to CONFLICT,
                    # else add to new_commit
                    if snapshot_branch[key] != snapshot_head[key]:
                        common_files.add(key)
                    else:
                        new_commit.append(snapshot_branch[key] + ' ' + key)

            # If common files exist -> CONFLICT, stop auto-merge
            if common_files:
                for file_name in common_files:
                    print("Auto-merging " + file_name)
                    print("CONFLICT (content): Merge conflict in " + file_name)

                    # read from both commits, get diff
                    file1 = snapshot_head[file_name]  # need to add /
                    file1 = OBJECT_PATH + '/' + file1[:2] + '/' + file1[2:]
                    file2 = snapshot_branch[file_name]
                    file2 = OBJECT_PATH + '/' + file2[:2] + '/' + file2[2:]
                    merged_content = diff_of_files(file1, file2, branch)

                    # Calculate hash, add to objects
                    _hash = get_hash(merged_content, by_content=True)
                    obj_folder = OBJECT_PATH + '/' + _hash[:2]
                    _path = obj_folder + '/' + _hash[2:]
                    if not os.path.exists(obj_folder):
                        os.mkdir(obj_folder)
                    with open(_path, "w") as f:
                        f.write(merged_content)

                    # Write new content to curr dir
                    with open(file_name, 'w') as f:
                        f.write(merged_content)

                    # Add to new_commit
                    new_commit.append(_hash + ' ' + file_name)
                print("Automatic merge failed; fix conflicts and "
                      "then commit the result.")
            else:
                # Auto-merge succeed, save new commit,
                # move current refs to new commit
                message = input("Please enter a commit message to explain"
                                " why this merge is necessary: ")
                commit_name = format_time(time.time(), floating=True)
                save_commit_information(author, commit_name, message)
                with open("%s/%s" % (SNAPSHOTS_PATH, commit_name), "w") as f:
                    f.write('\n'.join(new_commit))
                set_last_commit(branch, commit_name)
                set_last_commit(head_branch, commit_name)
                print("Auto-merge succeed.")
    update_index()
