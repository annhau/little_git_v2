import os

from lgit_helper import update_index, get_head, read_index_file

UNTRACKED_MESSAGE = "Untracked files:\n  (use \"./lgit.py add <file>...\" " \
                    "to include in what will be committed)\n"
NO_CHANGE_MESSAGE = "no changes added to commit (use \"./lgit.py " \
                    "add and/or \"./lgit.py commit -a\")"
NOT_STAGED_MESSAGE = "Changes not staged for commit:\n  (use \"./lgit.py add "\
                     "...\" to update what will be committed)\n  (use " \
                     "\"./lgit.py checkout -- ...\" to discard changes in " \
                     "working directory)\n "
STAGED_MESSAGE = "Changes to be committed:\n  (use \"./lgit.py reset HEAD" \
                 " ...\" to unstage)\n"
NOTHING_TO_COMMIT_MESSAGE = "nothing to commit (create/copy files and " \
                            "use \"./lgit.py add\" to track)\n"


def status():
    """
    Read /index, re-hash current files in it, compare to find which was
    changed from when they was added, which is not changed in staged for commit
    """
    ind_objects = update_index()
    staged_changes = []
    not_staged_changes = []
    committed = False

    for obj in ind_objects:
        if not obj.commit_hash.strip() or obj.add_hash != obj.commit_hash:
            # If added stages not commit, add to staged changes
            staged_changes.append(obj.file_name)
        if obj.commit_hash.strip():
            committed = True

        if obj.curr_hash != obj.add_hash:
            # added hash != current hash
            not_staged_changes.append(obj.file_name)

    # Print output
    print("On branch " + get_head())
    if not committed:
        print("\nNo commits yet\n")

    if not staged_changes and not not_staged_changes:
        print(NOTHING_TO_COMMIT_MESSAGE)

    # Print Staged changes
    if staged_changes:
        print(STAGED_MESSAGE)
        for change in staged_changes:
            print("\tmodified: %s" % change)
        print()

    # Print Not staged changes
    if not_staged_changes:
        print(NOT_STAGED_MESSAGE)
        for change in not_staged_changes:
            print("\tmodified: %s" % change)
        print()

        if not staged_changes:
            print(NO_CHANGE_MESSAGE)

    # Print untracked files
    untracked_files = get_untracked_files()
    if untracked_files:
        print(UNTRACKED_MESSAGE)
        for file in untracked_files:
            print('\t' + file)


def get_untracked_files():
    # Get untracked files in current dir that not in index
    curr_files = os.listdir('.')
    index_names = [x.file_name for x in read_index_file()]
    index_names = [x.split('/')[0] for x in index_names]
    return [x for x in curr_files if x not in index_names]
