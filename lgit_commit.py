import time

from lgit_helper import *


def commit(_message):
    if not is_any_to_commit():
        return

    # Save commit information and snapshots
    author = get_author_name()
    if author:
        commit_time = format_time(time.time(), floating=True)
        save_commit_information(author, commit_time, _message)
        save_snapshots(commit_time)

        current_branch = get_head()
        # Get last commit before save this commit to refs
        last_commit = get_last_commit(current_branch)
        set_last_commit(current_branch, commit_time)

        # If last_commit, save it and current commit to do backtrack
        if last_commit:
            save_commit_track(commit_time, last_commit)


def is_any_to_commit():
    ind_objects = read_index_file()
    for obj in ind_objects:
        if obj.add_hash != obj.commit_hash:
            return True
    return False


def get_last_commit(branch_name):
    try:
        with open('%s/%s' % (REFS_PATH, branch_name)) as f:
            last_commit = f.read().strip("\n")
    except FileNotFoundError:
        last_commit = None

    return last_commit


def set_last_commit(branch_name, commit_name):
    with open('%s/%s' % (REFS_PATH, branch_name), 'w') as f:
        f.write(commit_name + '\n')


def save_snapshots(c_time):
    # Save added stages to snapshots/c_time, update commit_field in /index
    ind_objects = read_index_file()

    snaps_content = []
    index_content = []

    for obj in ind_objects:
        # Get add hash and file name to save to snapshots
        added_hash = obj.add_hash
        file_name = obj.file_name

        obj.commit_hash = added_hash  # commit hash = added hash

        snaps_content.append(added_hash + ' ' + file_name)
        index_content.append(str(obj))

    # Save index
    write_file(INDEX_PATH, index_content)
    # Save snapshots
    write_file('%s/%s' % (SNAPSHOTS_PATH, c_time), snaps_content)


def save_commit_information(author, c_time, _message):
    # Save author, timestamp, message to /commits/c_time
    content = ['%s\n%s\n\n%s' % (author, c_time.split('.')[0], _message)]
    write_file('%s/%s' % (COMMITS_PATH, c_time), content)


def save_commit_track(curr, previous):
    with open(TRACKING_PATH, 'a') as f:
        f.write(curr + ' ' + previous + '\n')
