import os

from lgit_helper import LGIT_PATH


def init():
    if LGIT_PATH is not None:
        print("Git repository already initialized.")
        return

    # Make dir structure
    os.mkdir('.lgit')
    os.mkdir('.lgit/objects')
    os.mkdir('.lgit/commits')
    os.mkdir('.lgit/snapshots')
    os.mkdir('.lgit/refs')
    os.mkdir('.lgit/refs/heads')
    with open('.lgit/index', 'w'):
        pass
    with open('.lgit/config', 'w') as f:
        try:
            f.write(os.environ['LOGNAME'] + '\n')
        except KeyError:
            f.write("Default Author\n")
    with open('.lgit/HEAD', 'w') as f:
        f.write('master\n')
    with open('.lgit/commit_tracking', 'w'):
        pass
    with open('.lgit/refs/stash', 'w'):
        pass
