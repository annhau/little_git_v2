import datetime
import hashlib
import os
from glob import glob


class Index(object):
    def __init__(self, line):
        line = line.strip("\n")
        self.timestamp = line[:14]
        self.curr_hash = line[15:55]
        self.add_hash = line[56:96]
        self.commit_hash = line[97:137]
        self.file_name = line[138:]

    def __str__(self):
        return ' '.join([self.timestamp, self.curr_hash, self.add_hash,
                         self.commit_hash, self.file_name])


def copy_to_object(file_path):
    # Copy a file from file_path to OBJECT_PATH

    hash_code = get_hash(file_path)
    in_path = '%s/%s' % (OBJECT_PATH, hash_code[:2])
    if not os.path.exists(in_path):
        os.mkdir(in_path)

    # Make a copy of the file to objects folder
    out_path = '%s/%s' % (in_path, hash_code[2:])
    copy_file(file_path, out_path)


def get_hash(path_or_content, by_content=False):
    # Return hash or a file or a string
    if by_content:
        return hashlib.sha1(path_or_content.encode()).hexdigest()
    else:
        content = read_file(path_or_content)
        return hashlib.sha1(content.encode()).hexdigest()


def read_file(_path):
    # Return content of a file
    with open(_path) as f:
        content = f.read()
    return content


def write_file(_path, content, mode='w'):
    # Write content to _path
    if content:
        if not content[0]:
            return
        with open(_path, mode) as f:
            f.write('\n'.join(content) + '\n')


def copy_file(source, dest):
    # Copy a file from source to dest
    with open(source) as f1:
        with open(dest, 'w') as f2:
            content = f1.read()
            f2.write(content)


def list_files(_path):
    # List all file in a path recursively
    result = [y for x in os.walk(_path)
              for y in glob(os.path.join(x[0], '*'))] \
             + [y for x in os.walk(_path)
                for y in glob(os.path.join(x[0], '.*'))]
    return [i for i in result if os.path.isfile(i) or os.path.islink(i)]


def print_pathspec_error(_path):
    print("fatal: pathspec '%s' did not match any files" % _path)


def format_time(timestamp, floating=False):
    # Return a formatted string from a timestamp
    if floating:
        return datetime.datetime.utcfromtimestamp(timestamp) \
            .strftime("%Y%m%d%H%M%S.%f")
    return datetime.datetime.utcfromtimestamp(timestamp) \
        .strftime("%Y%m%d%H%M%S")


def format_time_from_string(time_string):
    # Return ctime from string
    return datetime.datetime.strptime(time_string, "%Y%m%d%H%M%S").ctime()


def get_lgit_path():
    # Find lgit path if exist in the current directory or its parents
    _path = os.path.realpath('.')
    while _path:
        if os.path.exists(_path + '/.lgit'):
            return _path, _path + '/.lgit'
        _path = '/'.join(_path.split('/')[:-1])
    return None, None


def read_index_file():
    # Read index file content, return list of Index object
    lines = read_file(INDEX_PATH).strip('\n')
    if lines:
        lines = lines.split("\n")
    indexes = [Index(line) for line in lines]
    return indexes


def update_index(filename=None):
    """
    Update INDEX and return list of index object after updated
    If file_name is specify, add or update file_name in /index
    If not, update_index was called by status, only update INDEX file
    """
    content = []  # holding index content after modify
    ind_objects = read_index_file()  # /index content
    filenames = [x.file_name for x in ind_objects]  # filenames in /index

    if filename and filename not in filenames:
        # File doesn't exist, append
        hash_code = get_hash(filename)
        time_stamp = format_time(os.path.getmtime(filename))
        arr = [time_stamp, hash_code, hash_code, ' ' * 40, filename]
        content = [str(x) for x in ind_objects] + [' '.join(arr)]
        write_file(INDEX_PATH, content)
    else:
        # File exist, update
        for obj in ind_objects:  # For obj in index content
            # re-hash to update current content
            obj.curr_hash = get_hash(obj.file_name)

            if filename and obj.file_name == filename:
                # Filename is provided, this is a lgit add operation
                obj.add_hash = obj.curr_hash

            content.append(str(obj))
        write_file(INDEX_PATH, content)

    return ind_objects


def get_head():
    # Get current branch
    with open(HEAD_PATH) as f:
        head = f.read().strip('\n')
    return head


def get_author_name():
    # Get author name from config
    author = read_file(CONFIG_PATH).strip('\n')
    return author


def set_author_name(author_name):
    # Set author name in config
    content = [author_name]
    write_file(CONFIG_PATH, content)


""" INIT VARIABLES --------------------------------------------------------"""

# Get .lgit path and its parent dir
LGIT_PARENT_PATH, LGIT_PATH = get_lgit_path()
INDEX_PATH = '%s/index' % LGIT_PATH
OBJECT_PATH = '%s/objects' % LGIT_PATH
SNAPSHOTS_PATH = '%s/snapshots' % LGIT_PATH
COMMITS_PATH = '%s/commits' % LGIT_PATH
CONFIG_PATH = '%s/config' % LGIT_PATH
HEAD_PATH = '%s/HEAD' % LGIT_PATH
REFS_PATH = '%s/refs/heads' % LGIT_PATH
TRACKING_PATH = '%s/commit_tracking' % LGIT_PATH
STASH_PATH = '%s/refs/stash' % LGIT_PATH
