import os

from lgit_helper import read_index_file, LGIT_PARENT_PATH


def ls_files():
    """
    list files in current folder
    :return a list of file_name
    """

    # Get current folder path by subtract its realpath with lgit_parent path
    current_folder = os.path.realpath('.') \
        .replace(LGIT_PARENT_PATH, '') \
        .strip('/')

    # List file in INDEX
    # Remove current folder name to get only filename
    ind_objects = read_index_file()
    file_names = []
    for obj in ind_objects:
        if current_folder in obj.file_name:
            file_name = obj.file_name.replace(current_folder, '').strip('/')
            file_names.append(file_name)

    # Sort output
    file_names.sort()
    return file_names
