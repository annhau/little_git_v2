import os

from lgit_helper import REFS_PATH, LGIT_PATH, get_head


def branch(_path):
    """ create new branch """
    # check if new branch is exists
    if os.path.exists('%s/%s' % (REFS_PATH, _path)):
        print('fatal: A branch named', _path, 'already exists.')
        return  # do nothing

    # get the last branch
    last_branch = get_head()

    # get the last commmit
    try:
        with open('%s/%s' % (REFS_PATH, last_branch)) as f:
            last_commit = f.read()
    except FileNotFoundError:
        print("fatal: Not a valid object name: '%s'." % last_branch)
        return

    # copy last commit to new branch
    with open('%s/%s' % (REFS_PATH, _path), 'w') as f:
        f.write(last_commit)


def list_branches():
    # List all current branch

    branches = os.listdir('%s/refs/heads' % LGIT_PATH)
    head = get_head()
    print('*' + '\033[92m', head, '\033[0m')
    if branches:
        branches.remove(head)
        branches = ['  ' + x for x in branches]
        print('\n'.join(branches))
