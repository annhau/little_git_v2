#!/usr/bin/env python3
import sys

from lgit_add import add
from lgit_branch import branch, list_branches
from lgit_checkout import checkout
from lgit_commit import commit
from lgit_helper import LGIT_PATH, set_author_name
from lgit_init import init
from lgit_log import log_commit
from lgit_ls import ls_files
from lgit_merge import merge
from lgit_rm import rm_file
from lgit_stash import stash, list_stash, apply_stash
from lgit_status import status

if __name__ == '__main__':
    if len(sys.argv) < 2:
        exit()

    command = sys.argv[1]
    arg_1 = sys.argv[2] if len(sys.argv) > 2 else None
    arg_2 = sys.argv[3] if len(sys.argv) > 3 else None

    if command == 'init':
        init()
    else:
        if LGIT_PATH is None:
            print("fatal: not a git repository "
                  "(or any of the parent directories)")
        else:
            if command == 'add':
                for path in sys.argv[2:]:
                    add(path)
            elif command == 'commit':
                if arg_2 and arg_1 == '-m':
                    message = arg_2
                    commit(message)
                else:
                    status()
            elif command == 'status':
                status()
            elif command == 'config' and arg_1 == '--author':
                set_author_name(arg_2)
            elif command == 'ls-files':
                files = ls_files()
                if files:
                    print('\n'.join(files))
            elif command == 'rm':
                for path in sys.argv[2:]:
                    rm_file(path)
            elif command == 'log':
                log_commit()
            elif command == 'branch':
                if arg_1:
                    branch(arg_1)
                else:
                    list_branches()
            elif command == 'checkout':
                checkout(arg_1)
            elif command == 'merge':
                merge(arg_1)
            elif command == 'stash':
                if not arg_1:
                    stash()
                elif arg_1 == 'list':
                    list_stash()
                elif arg_1 == 'apply':
                    apply_stash(arg_2)
            else:
                print("git: '%s' is not a git command. See 'git --help'."
                      % command)
