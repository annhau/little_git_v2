import os

from lgit_helper import read_index_file, INDEX_PATH, \
    list_files, print_pathspec_error


def rm_file(_path):
    """
    Remove file _path if it in index
    Update the index file after removed
    """
    # Read file names in index
    ind_objects = read_index_file()
    file_names = [x.file_name for x in ind_objects]

    if _path in file_names:
        if os.path.isfile(_path):
            # Remove file
            os.remove(_path)

            # Remove that file from index
            ind = file_names.index(_path)
            ind_objects.pop(ind)
            content = [str(obj) for obj in ind_objects]
            with open(INDEX_PATH, "w") as f:
                f.write("\n".join(content))
        else:
            # If _path is a folder, for every files in it, remove those files.
            if os.path.exists(_path):
                files = list_files(_path)
                for file in files:
                    rm_file(file)
            else:
                # Else, print a error
                print_pathspec_error(_path)
    else:
        print_pathspec_error(_path)
