# Team agreement


### The name of the team
> PT
### The working hours/days each member commits to
> 10 hours

### How often you will check-in with each others' progress
> 30 minutes

### The strengths and weaknesses of each member, and how to leverage/mitigate them for the project
> Good at coding -> CODEEEEE
### The allocation of the workload
> 50/50
### Your strategy so that you all understand the codebase
> Read code.

