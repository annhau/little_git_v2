import time

from lgit_helper import *


def checkout(branch):
    """ git checkout with BRANCH only """

    # checkout if branch not exists
    if not os.path.isfile('%s/%s' % (REFS_PATH, branch)):
        print("error: pathspec '%s' did not match any file(s) known to git."
              % branch)
        return
    # check if we already on target branch
    if branch == get_head():
        print("Already on '" + branch + "'")
        return

    # At first we need to delete all staged files from previous branch
    last_branch = get_head()

    # If HEAD and branch point on the same snapshot, just change the HEAD
    if read_file('%s/%s' % (REFS_PATH, last_branch))\
            == read_file('%s/%s' % (REFS_PATH, branch)):
        with open(HEAD_PATH, 'w') as f:
            f.write(branch)
        return

    # get all staged files of last branch
    last_staged_files = []
    with open('%s/%s' % (REFS_PATH, last_branch)) as f:
        last_snapshot = f.read().strip('\n')
        with open('%s/%s' % (SNAPSHOTS_PATH, last_snapshot)) as f_:
            for line in f_:
                if line[41:-1]:
                    last_staged_files.append(line[41:-1])

    # remove all the staged files
    # " we could do 'rsync' for later but i'm lazy
    # so later we have to rewrite all the files :p "
    files_modified = files_not_staged()
    if files_modified:
        print("error: Your local changes to the following files would"
              " be overwritten by checkout:")
        for file in files_modified:
            print('\t' + file)
        print("Please, commit your changes or stash them befor"
              "e you can switch branches.")
        print("Aborting")
        return

    for file in last_staged_files:
        if os.path.exists(file) and file not in files_modified:
            os.remove(file)

    print("Switched to branch '" + branch + "'")

    # we find the last snapshot of target branch and retrive contents from it
    # find the last commit/snapshots of branch
    commit = None
    with open('%s/%s' % (REFS_PATH, branch), 'r') as f:
        commit = f.read().strip('\n')

    # retrive the contents thourgh all files of last previous snapshot
    data_index = []
    with open('%s/%s' % (SNAPSHOTS_PATH, commit)) as f:
        for line in f:
            data = line
            hash = data[:40]  # get sha1 hash
            filepath = data[41:-1]  # get filename

            # rewrite the file :
            if len(filepath):
                with open(filepath, 'w') as f:
                    with open(OBJECT_PATH + '/' + hash[:2] + '/' + hash[2:],
                              'r') \
                            as f2:
                        f.write(f2.read())
                arr = [format_time(time.time()), hash, hash, hash, filepath]
                data_index.append(" ".join(arr) + '\n')

    # rewrite index file
    with open(INDEX_PATH, 'w') as f:
        f.write("".join(data_index))

    # reset HEAD pointing to new branch
    with open(HEAD_PATH, 'w') as f:
        f.write(branch)


def files_not_staged():
    ind_objects = update_index()
    not_staged_changes = []

    for obj in ind_objects:
        if obj.commit_hash != obj.curr_hash:
            not_staged_changes.append(obj.file_name)
    return not_staged_changes


def file_exist_on_branch(branch, file_check):
    with open(REFS_PATH + '/' + branch) as f:
        commit = f.read().strip('\n')

    with open(SNAPSHOTS_PATH + '/' + commit, 'r') as f:
        for line in f:
            filename = line.strip().split(' ')[-1]
            if filename == file_check:
                return True
    return False
