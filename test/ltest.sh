#!/usr/bin/env bash
rm -r .lgit
echo abc > abc
echo xyz > xyz
../lgit.py init
../lgit.py status
../lgit.py add abc
../lgit.py status
../lgit.py commit -m "abc"

../lgit.py branch new
../lgit.py checkout new
echo "modified abc" > abc
../lgit.py add xyz abc
../lgit.py commit -m "xyz"
echo "newfile" > newfile
../lgit.py add newfile
../lgit.py commit -m "newfile"

../lgit.py checkout master
echo "edit newfile" > newfile
echo "edit abc" > abc
../lgit.py add newfile abc
../lgit.py commit -m "newfile"

../lgit.py branch
../lgit.py merge new
