#!/usr/bin/env bash
rm -r .lgit

echo abc > abc
echo xyz > xyz
../lgit.py init
../lgit.py add abc
../lgit.py commit -m "abc"

../lgit.py branch new
../lgit.py checkout new
../lgit.py add xyz
../lgit.py commit -m "xyz"

../lgit.py branch master
../lgit.py checkout master
echo xyz2 > xyz2
../lgit.py add xyz2
../lgit.py commit -m xyz2

../lgit.py merge new
