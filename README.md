# LGit

This project implements a mini version of Git. I learned how Git constructs their files,
how they detect if a file was changed, how they're able to revert it, so on... 

![img.png](img.png)

## Run
> python lgit.py [command]

or follow example in test folder.


## Commands
- init
- add
- commit
- status
- config --author
- ls-files
- rm 
- log
- branch
- checkout
- merge
- stash
  - list
  - apply




