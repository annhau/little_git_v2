from lgit_helper import *


def add(_path):
    # lgit add: add a file to .lgit folder with name is its hash
    if os.path.isfile(_path):
        # Copy the file to objects
        copy_to_object(_path)

        # Update index file
        update_index(_path)
    else:
        # If _path is a folder, for every files in it, add those files.
        if os.path.exists(_path):
            files = list_files(_path)
            for file in files:
                add(file)
        else:
            # Else, print a error
            print_pathspec_error(_path)
