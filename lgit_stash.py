"""
Workflow:
git stash: save current hash in INDEX to a snapshot. Save that snapshot to
    refs/stash as "stash@{number} snapshot"
    Reverse current directory to last commit's snapshot
git stash list: list all stash in refs/stash
git apply stash_name: Reverse current dir by snapshot on refs/stash
"""
import time

from lgit_helper import *
from lgit_merge import load_snapshot


def read_stash():
    # Return {stash name: snapshot}
    dic = {}
    with open(STASH_PATH) as f:
        for line in f:
            key, value = line.split()
            dic[key] = value
    return dic


def list_stash():
    # Print stash file
    with open(STASH_PATH) as f:
        print(end=f.read())


def save_stash(snapshot):
    # Save snapshot to stash file, with name is the last index
    stashes = read_stash()
    name = "stash@{%d}" % len(stashes)
    with open(STASH_PATH, "a") as f:
        f.write(name + ' ' + snapshot + "\n")


def save_new_snapshot(content):
    # Write content to a new snapshot file in current time
    snapshot_name = format_time(time.time(), floating=True)
    with open("%s/%s" % (SNAPSHOTS_PATH, snapshot_name), "w") as f:
        f.write('\n'.join(content))
    return snapshot_name


def apply_stash(stash_name):
    # Apply a stash to current dir

    stash_dict = read_stash()
    if stash_name in stash_dict:
        snapshot = stash_dict[stash_name]
    else:
        print("fatal: ambiguous argument '%s': unknown revision or path not"
              " in the working tree." % stash_name)
        return

    # Reverse snapshot
    with open("%s/%s" % (SNAPSHOTS_PATH, snapshot)) as f:
        for line in f:
            _hash, filename = line.split()
            obj_folder = OBJECT_PATH + '/' + _hash[:2]
            _path = obj_folder + '/' + _hash[2:]
            copy_file(_path, filename)


def stash():
    """
    This function looks for any change made in the index file, save them
    to a new snapshot that doesn't belong to any commit. The snapshot will
    link to a stash_name that we can apply it later.
    """
    head = get_head()
    update_index()
    ind_objects = read_index_file()

    any_change = any([obj.curr_hash != obj.commit_hash for obj in ind_objects])
    if not any_change:
        print("No local changes to save")
        return

    new_snapshot = [obj.curr_hash + ' ' + obj.file_name for obj in ind_objects]

    snapshot_name = save_new_snapshot(new_snapshot)
    save_stash(snapshot_name)

    # Remove current tracked files in index
    index_files = [obj.file_name for obj in ind_objects]
    for file in index_files:
        os.remove(file)

    # Reverse current commit's snapshot
    curr_snapshot = load_snapshot(head)
    for file in curr_snapshot:
        _hash = curr_snapshot[file]
        _path = OBJECT_PATH + '/' + _hash[:2] + '/' + _hash[2:]
        copy_file(_path, file)

    # Update current index
    with open(INDEX_PATH, "w") as f:
        for file in curr_snapshot:
            _hash = curr_snapshot[file]
            f.write(' '.join([snapshot_name[:14], _hash, _hash, _hash, file])
                    + "\n")
